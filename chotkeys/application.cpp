/*
 * Copyright (C) 2021 CutefishOS Team.
 *
 * Author:     Reion Wong <aj@cutefishos.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"
#include "hotkeys.h"

#include <QDebug>
#include <QFileSystemWatcher>

Application::Application(QObject *parent)
    : QObject(parent)
    , m_hotKeys(new Hotkeys)
{
    // load settings
    initSettings();

    m_settingsPath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/.config/cutefish/shortcuts.conf";

    connect(m_hotKeys, &Hotkeys::pressed, this, &Application::onPressed);
    connect(m_hotKeys, &Hotkeys::released, this, &Application::onReleased);
}

bool Application::isValidSettingGroup(const QString group)
{
    QStringList validGroups = { "Screenshot", "Screenlocker", "Terminal", "Launcher", "Filemanager", "Debinstaller", "Shutdown" };

    return validGroups.contains(group);
}


void Application::setupShortcut(QKeySequence keySeq)
{
    m_hotKeys->registerKey(keySeq);
}

void Application::initSettings()
{
    QFile file(m_settingsPath);
    QSettings settings(m_settingsPath, QSettings::IniFormat);

    if (!file.exists())
    {
        settings.beginGroup("Screenshot");
        settings.setValue("Exec", "cutefish-screenshot");
        settings.setValue("KeySeq", "Ctrl+Alt+A");
        settings.endGroup();

        settings.beginGroup("Screenlocker");
        settings.setValue("Exec", "cutefish-screenlocker");
        settings.setValue("KeySeq", "Meta+L");
        settings.endGroup();

        settings.beginGroup("Terminal");
        settings.setValue("Exec", "cutefish-terminal");
        settings.setValue("KeySeq", "Ctrl+Alt+T");
        settings.endGroup();

        settings.beginGroup("Launcher");
        settings.setValue("Exec", "cutefish-launcher");
        settings.setValue("KeySeq", "Meta");
        settings.endGroup();

        settings.beginGroup("Filemanager");
        settings.setValue("Exec", "cutefish-filemanager");
        settings.setValue("KeySeq", "Ctrl+Alt+F");
        settings.endGroup();

        settings.beginGroup("Debinstaller");
        settings.setValue("Exec", "cutefish-debinstaller");
        settings.setValue("KeySeq", "Ctrl+Alt+I");
        settings.endGroup();

        settings.beginGroup("Shutdown");
        settings.setValue("Exec", "cutefish-shutdown");
        settings.setValue("KeySeq", "Ctrl+Alt+Del");
        settings.endGroup();
    }

    all = settings.childGroups();
    for (int i = 0; i < all.size(); ++i)
    {
        // validate the group name is a valid group
        if (!isValidSettingGroup(all.at(i)))
        {
            // invalid group, don't process
            continue;
        }

        qDebug() << all.at(i);

        settings.beginGroup(all.at(i));
        QStringList keys = settings.childKeys();

        // make sure we have the required values
        if (!keys.contains(("Exec")))
        {
            qDebug() << "Exec not found for shortcut setting";
            continue;
        }

        if (!keys.contains(("KeySeq")))
        {
            qDebug() << "KeySeq not found for shortcut setting";
            continue;
        }

        // add our short cut to our map
        QString keySeq = settings.value("KeySeq").toString();

        // make sure we don't register a shortcut twice
        if (!shortcuts.contains(keySeq))
        {
            shortcuts.insert(keySeq, settings.value("Exec").toString());

            // setup our shortcut
            setupShortcut(QKeySequence(keySeq));
        }

        settings.endGroup();
    }


    // register any key codes seperately
    m_hotKeys->registerKey(647);
}

void Application::cleanSetting()
{
    // do nothing for now
}


void Application::onPressed(QKeySequence keySeq)
{
    if (shortcuts.contains(keySeq.toString()))
    {
        QString exec = shortcuts.value(keySeq.toString());
        QProcess::startDetached(exec, QStringList());
    }
}

void Application::onReleased(QKeySequence keySeq)
{
    // do nothing for now
}