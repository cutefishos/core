/*
 * Copyright (C) 2021 CutefishOS Team.
 *
 * Author:     Reion Wong <aj@cutefishos.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QObject>
#include <QProcess>
#include <QKeySequence>
#include <QSettings>
#include <QStandardPaths>
#include <QFile>
#include <QMap>
#include "hotkeys.h"

class Application : public QObject
{
    Q_OBJECT

public:
    explicit Application(QObject *parent = nullptr);

private:
    void setupShortcut(QKeySequence keySeq);
    void initSettings();
    bool isValidSettingGroup(const QString group);

private slots:
    void onPressed(QKeySequence keySeq);
    void onReleased(QKeySequence keySeq);

private:
    Hotkeys *m_hotKeys;
    void cleanSetting();
    QString m_settingsPath;
    QStringList all;
    QMap<QString, QString> shortcuts;
    QSettings settings;
};

#endif // APPLICATION_H
